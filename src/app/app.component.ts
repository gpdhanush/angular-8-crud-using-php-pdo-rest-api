import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {ReadService} from './_services/read.service';
import {MatDialog, MatDialogConfig, MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {AddNewRecordComponent} from './add-new-record/add-new-record.component';
import {UpdateRecordsComponent} from './update-records/update-records.component';
import {ViewRecordsComponent} from './view-records/view-records.component';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'angularCRUD';
  displayedColumns: string[] = ['s_no', 'emp_name', 'emp_id', 'emp_mobile', 'emp_email', 'emp_dob', 'action'];
  dataSource = new MatTableDataSource();
  dialogConfig: MatDialogConfig;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  constructor(private fb: FormBuilder, private readService: ReadService,
              private dialog: MatDialog,  private matSnackBar: MatSnackBar, private spinner: NgxSpinnerService) {
  }

  ngOnInit(): void {
    this.getUser();
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }
  getUser() {
    this.spinner.show();
    this.readService.getUserDetails().subscribe(res => {
      if (res.message) {
        this.spinner.hide();
        this.openSnackbar(res.message, 'Close');
      } else {
        this.spinner.hide();
        this.dataSource.data = res;
      }
    }, error => {
      console.log(error);
      this.spinner.hide();
      this.openSnackbar('Something wrong!', '');
    });
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  addNewRecords() {
    const dialogRef = this.dialog.open(AddNewRecordComponent, this.dialogConfig = {
      height: '500px',
      width: '600px',
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== '' || result !== undefined) {
        if ( result === 'close' || result === undefined) {
          return false;
        } else {
          this.spinner.show();
          this.readService.insertRecord(result).subscribe(res => {
            this.spinner.hide();
            this.openSnackbar(res.message, 'Close');
            this.getUser();
          });
        }
      }
    });
  }
  openSnackbar(message, action?) {
    this.matSnackBar.open(message, action, {
      verticalPosition: 'bottom',
      horizontalPosition: 'center',
      duration: 3000,
    });
  }
  deleteUser(uid: any) {
    const userId = {id : parseInt(uid, 10)};
    console.log(JSON.stringify(userId));
    this.spinner.show();
    this.readService.deleteRecord(JSON.stringify(userId)).subscribe(res => {
      this.spinner.hide();
      this.openSnackbar(res.message, 'Close');
      this.getUser();
    }, error => {
      this.spinner.hide();
      this.openSnackbar('Something wrong!', 'Close');
    });
  }
  disabledUser(uid: any) {
    const userId = {id : uid};
    this.spinner.show();
    this.readService.disableUser(userId).subscribe(res => {
      this.spinner.hide();
      this.openSnackbar(res.message, 'Close');
      this.getUser();
    }, error => {
      this.spinner.hide();
      this.openSnackbar('Something wrong!', 'Close');
    });
  }
  viewUserDetails(uid: any) {
    const dialogRef = this.dialog.open(ViewRecordsComponent, this.dialogConfig = {
      height: '500px',
      width: '600px',
      data: uid
    });
  }
  updateUser(value) {
    const dialogRef = this.dialog.open(UpdateRecordsComponent, this.dialogConfig = {
      height: '500px',
      width: '600px',
      data: value
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== '' || result !== undefined) {
        if ( result === 'close' || result === undefined) {
          return false;
        } else {
          this.spinner.show();
          this.readService.updateRecords(result).subscribe(res => {
            this.spinner.hide();
            this.openSnackbar(res.message, 'Close');
            this.getUser();
          });
        }
      }
    });
  }
}
