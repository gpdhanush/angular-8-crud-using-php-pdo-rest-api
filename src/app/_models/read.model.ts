export interface User {
  emp_id: string;
  emp_name: string;
  emp_role: string;
  emp_mobile: string;
  emp_email: string;
  emp_dob: string;
  emp_status: string;
}
